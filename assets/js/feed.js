var domainFeeJs ;
function feedFunction(data) {
  $('#plus_img').click(function(){
    $('#add_img').click();
  })
  $('#plus_imgex').click(function(){
    $('#file_infor_feed').click();
  })
  var len = data.length;
  var events = [];
  for (i = 0; i < len; i++) {
    events.push({
      id : data[i].id,
      title: data[i].title,
      start: data[i].date_start+' '+data[i].time_start,
      end: data[i].date_end+' '+data[i].time_end,
      imageurl: data[i].file,
      // description: data[i].description,
      description: data[i].feed_emoji
    });
  }
  var array_id = [];
  $(function () {
    /* initialize the external events
     -----------------------------------------------------------------*/
    function init_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex: 1070,
          revert: true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    init_events($('#external-events div.external-event'))

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)

    var date = new Date()
    var d = date.getDate(),
      m = date.getMonth(),
      y = date.getFullYear()
    $('#calendar').fullCalendar({

      header: {
        left: 'prev title next',
        right: 'month'
      },
      buttonText: {
        month: 'MONTH',
      },
      //Random default events
      events: events,
      eventRender: function (event, eventElement) {
        eventElement.append('<div class="content_event"><img style="float: right;height: 16px;cursor:pointer" src="assets/images/icon/close.png" class="remove_event"  data-id="'+event.id+'"><span class="time">' + event.start.format('hh:mma') + '-' + event.end.format('hh:mma') + '</span>' +
          '<div style="text-overflow: ellipsis"><div class="img_event mr-3"><img src="' + event.imageurl + '" width=' + 50 + ' height=' + 40 + '></div>' +
          '<div class="content"><div class="font-KPMGBold"><span>' + event.title + '</span></div><div><span class="description_icon"></span></div></div></div>' +
          '</div>');

        eventElement.find('.description_icon').append(event.description);

      },
    })


    $('.fc-left').append('<button type="button" data-toggle="modal" data-target="#modal-addfeed" class="button_addfeed fc-addFeed-button fc-button fc-state-default fc-corner-left fc-corner-right" id="add_feed_button">ADD FEED</button>');
    $('.fc-left').append('<button type="button"  class="button_addfeed fc-addFeed-button fc-button fc-state-default fc-corner-left fc-corner-right " id="button_event" >EVENT</button>');
    $('.fc-right').append('<button type="button" class="button_list fc-list-button fc-button fc-state-default fc-corner-right">LIST</button>')


  })

  $(document).ready(function () {
    // $('.datepicker').datetimepicker({
    //   format:'YYYY-MM-DD'
    // });
    // $('.datepicker_external').datetimepicker({
    //   format:'YYYY-MM-DD HH:mm'
    // });
    // $('.timepicker').datetimepicker({
    //   format:'HH:mm',
    //   sideBySide: false,
    // });

    // function css() {
    //   $('.recurrence').css('color', '#c3bfbf').css('background-color', '#dedbdb');
    // }

    $('.recurrence').each(function (a) {
      var array = [];
      array.push(a);
      $(this).click(function () {
        css();
        $('#choose' + a).css("color", "black").css('background-color', '#ebe7e7ad')
      })
    });
    $('#add_feed').click(function () {
      var title = $('#title').val();
      var description = $('#description').val();
      var start_day = $('#start_day').val();
      var end_day = $('#end_day').val();
      var img = $('.blah0').attr('src');

      $('#calendar').fullCalendar('renderEvent', {
        title: title,
        start: moment(start_day).format('YYYY-MM-DD') + ' 08:00',
        end: moment(end_day).format('YYYY-MM-DD') + ' 22:00',
        imageurl: img,
        description: description
      },true);

      $('#calendar').fullCalendar('destroy')


    });
    var array = [];
    $('.button_list').click(function () {
      $('.fc-view-container').css('display', 'none');
      $('.fc-month-button').removeClass("fc-state-active");
      $('button.fc-prev-button').css('display', 'none');
      $('.fc-left h2').addClass('invisible')
      $('button.fc-next-button').css('display', 'none');
      $(this).addClass('fc-state-active');
      $('.card_list').css('display', 'block');
      var a = $('.fc-left h2').text();
      array = [];
      array.push(a);
      // $('.fc-left h2').text('August 2018');
    });

    $('.fc-month-button').click(function () {
      monthClick();
    });

  })


}
function monthClick(){
  $('.card_list').css('display', 'none');
  $('.fc-view-container').css('display', 'block');
  $('button.fc-prev-button').css('display', 'block');
  $('button.fc-next-button').css('display', 'block');
  $('.fc-left h2').css('display', 'block');
  $('.fc-left h2').removeClass('invisible')
  $('.fc-month-button').addClass("fc-state-active");
  $('.button_list').removeClass("fc-state-active");
  // $('.fc-left h2').text(array[0]);
}



var url = window.location.pathname;
var id = url.substring(url.lastIndexOf('/') + 1);
var formData = new FormData();

// arrImg.push($('.item').length);
function readURL(input,bla) {
  if (input.files && input.files[0]) {
    var reader = new FileReader()
    reader.onload = function (e) {
      $(bla)
        .attr('src', e.target.result)
        .width(270)
        .height(180);
      $(bla+'_plus_img').css('display', 'none');
      $('#plus_imgex').css('display', 'none!important');
      $('#btn_addimg').css('display', 'none');
      $(bla+'_add_img').css('display', 'none');
    };
    reader.readAsDataURL(input.files[0]);
    formData.append('file[]', input.files[0]);
  }
  if (input.files.length === 0) {
    $(bla).attr('src', "").removeAttr('style');
    $(bla+'_plus_img').css('display', 'inline-block');
    $('#plus_imgex').css('display', 'inline-block');
    $('#btn_addimg').css('display', 'block');
    $(bla+'_add_img').css('display', 'inline-block');
  }
}

function getselect2(ip){
  $.ajax({
    url: ip + "/api/tag/all",
    type: 'GET',

    success: function (data) {
      var html = '';
      $.each(data.data, function (key, value) {
        html += "<option value='" + value.id + "'>" + value.name + "</option>";
      });
      $(".myTags").html(html)
      $('.myTags').select2();
    }
  });
}

function setTag(id, domain) {
  $.ajax({
    url: domain + '/api/feed_data/show/'+id,
    type:'GET',
    success:function (data) {
      var arr = []
      for(i =0; i<data.data.detailfavorite.length;i++){
       arr.push(data.data.detailfavorite[i].favorite_id)
      }
      $('#favorite_feedUpdate').val(arr);
      $('#favorite_feedUpdate').select2();
    }
  })
}

function setTagZone(id ,domain) {
  $.ajax({
    url: domain + '/api/config/zone/show/'+id,
    type:'GET',
    success:function (data) {
      var arr = []
      for(i =0; i<data.data.favorites.length;i++){
       arr.push(data.data.favorites[i].id)
      }
      $('#favorite_update').val(arr);
      $('#favorite_update').select2();
    }
  })
}
// function recurrence_hide() {
//   var favorite_val = $('#favorite_feed').val();
//   if(favorite_val ==""){
//     $('#recurrence_feed').show();
//   }else{
//     $('#recurrence_feed').hide();
//   }
// };
// function recurrenceUpdate_hide() {
//   var favorite_val = $('#favorite_feedUpdate').val();
//   if(favorite_val ==""){
//     $('#recurrence_feedUpdate').show();
//   }else{
//     $('#recurrence_feedUpdate').hide();
//   }
// };
function removeclass(){
  $('#content_chat_addFeed').focus(function(event) {
    $('#cke_content_chat_addFeed').remove();
  });
  $('#content_chat_updateFeed').focus(function(event) {
    $('#cke_content_chat_updateFeed').remove();
  });
  $('#content_chat').focus(function(event) {
    $('#cke_content_chat').remove();
  });
}
function show_table_emojiAddfeed(class_emoji){
  // thêm hình vào contenteditable có dùng function l?y v? trí cursor
  var id_dem = 0;
  var id_dem1 = 0;
  var arr = [];
  for (i = 0; i < 51; i++) {
    id_dem1 = i;
    for (j = 0; j < 52; j++) {
      arr.push(id_dem1);
      id_dem1 += 51;
    }
  }
  for (i = 0; i < 100; i += 1.96078) {
    for (j = 0; j < 100; j += 1.96078) { // if(id_dem==12){ // $('.emoji-mart-scroll').append('<p>asdasd</p>');
      // }
      switch (id_dem) {
        case 0:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Number</h4>')
          break;
        case 12:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Flag</h4>');
          break;
        case 289:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Travel & Places</h4>');
          break;
        case 347:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Food & Drink</h4>');
          break;
        case 435:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Entertainment & Sports </h4>');
          break;
        case 654:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Animals</h4>');
          break;
        case 718:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Objects </h4>');
          break;
        case 801:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>People</h4>');
          break;
        case 1282:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Objects 2</h4>');
          break;
        case 1584:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Smileys</h4>');
          break;
        case 1776:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Traffic</h4>');
          break;
        case 1948:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Objects 3</h4>');
          break;
        case 2092:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Sports 2</h4>');
          break;
        case 2168:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Activity & Fruites</h4>');
          break;
        case 2208:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Animals 2</h4>');
          break;
        case 2233:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>People 2</h4>');
          break;
        case 2423:
          $('.emoji-mart-scroll-'+class_emoji).append('<h4>Symbols</h4>');
          break;
      }
      if (i > 98 && j > 54 || i > 99) {
        id_dem++;
      } else {
        $('.emoji-mart-scroll-'+class_emoji).append('<button class="emoji-mart-'+class_emoji+'" id="'+class_emoji+ + arr[id_dem] +
          '" style="background-position: ' + i + '% ' + j + '%"></button>');
        id_dem++
      }
    }
  }
  $('.emoji-mart-'+class_emoji).each(function(a){
    var id_emoji = this.id;

    $('#'+id_emoji).click(function(){

      var icon_position = this.style.backgroundPosition;

      // $('#content_chat').focus();
      $('#content_chat_'+class_emoji).focus();
      $('#emoji_custom_'+class_emoji).collapse('hide');
      var src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
      document.execCommand("insertHTML", false,"<img class=icon_emoji src=data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7 id=icon_emoji"+id_emoji+">");
      $('<style>#icon_emoji'+id_emoji+'{background-position:'+icon_position+'}</style>').appendTo('body')
    });
  })
}

function show_emoji_feed(content) {
  var val= content;
  if(val) {
    var number_icon = val.match(/(\d+)/g);
    if(number_icon != null){
      for(i=0; i<number_icon.length; i++){
        var icon_position = $('#'+number_icon[i]).attr("style");
        $('<style>#icon_emojiaddFeed'+number_icon[i]+'{'+icon_position+'}</style>').appendTo('body')
      }
      var txt_content = val.replace(/sprite index=/g,'img class="icon_emoji" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id=icon_emojiaddFeed').replace(/color=/g,'font color=').replace(/[/]color/g,'/font')
      $('#content_chat').html(txt_content);
      console.log(val)
      console.log(txt_content)
    }

  }

}

function show_icon(data){
  var arr_icon= []
  for(dem=0; dem<data.length; dem++){
    var val = data[dem].feed_emoji;
    if(val) {
      var number_icon = val.match(/(\d+)/g);
      if(number_icon != null){
        let id_icon_feed = number_icon.filter((v,j) => number_icon.indexOf(v) === j)
        arr_icon = arr_icon.concat(id_icon_feed)
      }
    }
  }

  let arr_icon_feed = arr_icon.filter((v,j) => arr_icon.indexOf(v) === j)
  for(i=0; i<arr_icon_feed.length; i++){
    var icon_position = $('#'+arr_icon_feed[i]).attr("style");
    $('<style>#icon_emojiupdateFeed'+arr_icon_feed[i]+'{'+icon_position+'}</style>').appendTo('body')
  }
}

function show_anypicker(){
  // $('.timepicker').AnyPicker(
  //   {
  //     mode: "datetime",
  //
  //     dateTimeFormat: "HH:mm",
  //
  //     theme: "Android" // "Default", "iOS", "Android", "Windows"
  //   });
  $('.timepicker').datetimepicker(
    {
      format: 'HH:mm',
      sideBySide: true
    });
  $('.datepicker').datetimepicker({
    format:'YYYY-MM-DD'
  });
  $('.datepicker_external').datetimepicker({
    format:'YYYY-MM-DD HH:mm'
  });
}
