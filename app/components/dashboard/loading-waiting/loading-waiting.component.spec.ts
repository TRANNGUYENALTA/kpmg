import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingWaitingComponent } from './loading-waiting.component';

describe('LoadingWaitingComponent', () => {
  let component: LoadingWaitingComponent;
  let fixture: ComponentFixture<LoadingWaitingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingWaitingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingWaitingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
