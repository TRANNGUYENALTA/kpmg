import {Component, OnInit, OnChanges, AfterViewInit, ViewChild} from '@angular/core';
import {HomeService} from '../../../../../services/home/home.service';
import {SearchProfileService} from '../../../../../services/searchProfile/search-profile.service';
import {CurrentReportService} from '../../../../../services/report/current-report.service';
import {Subscription} from 'rxjs';
import {CurrentReportComponent} from '../current-report/current-report.component';
import {DetailPersonService} from '../../../../../services/detailPerson/detail-person.service';
import {MailService} from '../../../../../services/mail/mail.service';
import {fabric} from 'fabric';
// Data table
import {ChangeDetectorRef} from '@angular/core';
import swal from 'sweetalert';
// Config Ip
import {ConfigIpService} from '../../../../../services/configIP/config-ip.service';

declare var $: any;
declare var setHeightElement: any;
declare var mapAction: any;
declare var drawChart1: any;
declare var datas: any;
declare var clickItem: any;
declare var openNav: any;
declare var closeNav: any;
declare var ckEditor: any;

@Component({
  selector: 'app-home-content',
  templateUrl: './home-content.component.html',
  styleUrls: ['./home-content.component.css']
})
export class HomeContentComponent implements OnInit, OnChanges, AfterViewInit {
  public curWidth: any = 0;
  public curHeight: any = 0;
  public allFunction: any;
  public subscription: Subscription;
  public functionsSearch: any = '';
  public title = '';
  public infor: any = '';
  public logs: any = '';
  public customersArr: any = [];
  public isOpenSidebar: any = false;
  public curID: 0;
  public curZoom: any = 1;
  public scaleRatio: any = 0.2;
  public zonePosition: any;
  // Ip
  public ip: any = '';

  canvas: any;
  // Send Mail
  public email: any = '';
  public description: any = '';
  // first Get
  public firstGetPerson = true;
  // is loading
  public isLoading = true;

  @ViewChild(CurrentReportComponent) currentReport;

  constructor(public homeService: HomeService,
              public searchProfileService: SearchProfileService,
              public currentService: CurrentReportService,
              public detailPersonService: DetailPersonService,
              private chRef: ChangeDetectorRef,
              private mailService: MailService,
              private configIp: ConfigIpService) {
    this.ip = configIp.getIp();
  }

  ngOnInit() {
    const that = this;
    // CK editor
    ckEditor('editor1');
    ckEditor('editor2');
    const myImg = $('#myCanvas');
    this.curWidth = myImg.width();
    this.curHeight = myImg.height();
    this.setTitleLive();
    this.getAllFunction();

    this.getPersonsOnMap([]);
    this.getZonePosition();
    // Draw images canvas
    this.canvas = new fabric.Canvas('myCanvas');
    this.canvas.renderAll();
    // End Draw background images canvas

    mapAction();
    $('.carousel').carousel({
      interval: 0
    });
    $('#live-tab').on('click', function () {
      that.setTitleLive();
    });

    // search
    $('#multi-select').dropdown();
    setHeightElement('#map-office');
    setHeightElement('#myTabContent');
    clickItem();
    // this.getAllCustomerHome([]);

    that.canvas.on('mouse:down', function (options) {
      if (options.target && options.target.id) {
        that.isLoading = true;
        that.getInforPerson(options.target.id);
        that.destroyDataTable('#logTable');
        that.getLogPerson(options.target.person_id);
        if (that.isOpenSidebar) {
          if (that.curID === options.target.id) {
            that.isOpenSidebar = false;
            closeNav('#member-detail');
          } else {
            that.curID = options.target.id;
            openNav('#member-detail');
          }
        } else {
          that.isOpenSidebar = true;
          that.curID = options.target.id;
          openNav('#member-detail');
        }
        closeNav('#list-profile');
      }
    });

    $(document).on('click', '#zoom-in', function () {
      that.curZoom += that.scaleRatio;
      that.setWidthHeightCanvas();
      that.loadPerson();
    });
    $(document).on('click', '#zoom-out', function () {
      that.curZoom -= that.scaleRatio;

      that.setWidthHeightCanvas();
      that.loadPerson();
    });
  }

  setWidthHeightCanvas() {
    const newWidth = this.curWidth * this.curZoom;
    const newHeight = this.curHeight * this.curZoom;
    this.canvas.setWidth(newWidth);
    this.canvas.setHeight(newHeight);

    this.setBackgroundImage();
    this.canvas.renderAll();
  }

  loadPerson() {
    this.removeAll();
    this.loadZones(this);
    this.setBackgroundImage();
    if (this.curZoom >= 1.4) {
      this.loadImagePerson(this);
    } else {
      this.loadCirclePerson(this);
    }
  }

  // Draw background images
  setBackgroundImage() {
    const imageUrl = './assets/images/5x.jpg';
    const that = this;
    fabric.Image.fromURL(imageUrl, function (img) {
      that.canvas.setBackgroundImage(img, that.canvas.renderAll.bind(that.canvas), {
        scaleX: that.canvas.width / img.width,
        scaleY: that.canvas.height / img.height
      });
    });

    this.canvas.renderAll();
  }

  loadCirclePerson(that) {
    this.customersArr.forEach(function (item, i) {
      const circle = new fabric.Circle({
        top: item.top * that.curZoom,
        left: item.left * that.curZoom,
        radius: 7,
        fill: item.color,
        hasControls: false
      });
      circle.set({
        id: item.id,
        person_id: item.person_id,
      });
      that.canvas.add(circle);
    });
    this.isLoading = false;
  }

  loadZones(that) {
    this.setBackgroundImage();
    this.zonePosition.forEach(function (item, i) {
      const labeledRect = new fabric.Rect({
        width: 140 * that.curZoom,
        height: 30 * that.curZoom,
        left: -70 * that.curZoom,
        top: 10 * that.curZoom,
        fill: '#000',
        opacity: 0.5,
        rx: 8,
        ry: 8,
        selectable: false
      });

      const circle = new fabric.Circle({
        radius: 15 * that.curZoom,
        fill: item.color,
        originX: 'center',
        originY: 'center'
      });
      const count = '' + item.count;
      const text = new fabric.Text(count, {
        fontSize: 15 * that.curZoom,
        fill: '#FFF',
        originX: 'center',
        originY: 'center',
        fontFamily: 'KPMG',
        fontWeight: 'bold',
      });

      const textName = new fabric.Text(item.name, {
        fontSize: 12 * that.curZoom,
        fill: '#FFF',
        originX: 'center',
        originY: 'center',
        top: 25 * that.curZoom,
        fontFamily: 'KPMG',
        fontWeight: 'bold',
        textTransform: 'uppercase'
      });


      const left = item.left_from + ((item.left_to - item.left_from) / 2);
      const top = item.top_from;

      const group = new fabric.Group([labeledRect, textName, circle, text], {
        left: left * that.curZoom,
        top: top * that.curZoom,
        originX: 'center',
        // originY: 'center',
        hasControls: false
      });

      that.canvas.add(group);
    });
  }

  loadImagePerson(that) {
    this.customersArr.forEach(function (item, i) {
      fabric.util.loadImage(item.file, function (img) {
        const legimg = new fabric.Image(img, {
          id: item.id,
          left: item.left * that.curZoom,
          top: item.top * that.curZoom,
          scaleX: (40 / img.width) * that.curZoom,
          scaleY: (40 / img.height) * that.curZoom,
          hasControls: false,
          // stroke: item.color,
          // strokeWidth: 20,
          // fill: 'rgba(0,0,200,0.5)'
          // clipTo: function (ctx) {
          //   // ctx.arc(0, 0, 75, 0, Math.PI * 2, true);
          //   ctx.arc(100, 75, 50, 0, 2 * Math.PI);
          // }
        });

        legimg.set({
          id: item.id,
          person_id: item.person_id,
          class: 'circle-image',
        })
        ;
        that.canvas.add(legimg);
        that.canvas.renderAll();
      });
    });
    this.isLoading = false;
  }

  // Delete canvas
  removeAll() {
    this.canvas.remove(...this.canvas.getObjects());

  }

  // End draw background images

  getPersonsOnMap(data) {
    this.isLoading = true;
    this.homeService.getCurrentPersonOnMap(data).subscribe(res => {
      this.customersArr = res.data;
      this.loadPerson();
    }, err => {
      console.log(err);
    });
  }

  getZonePosition() {
    this.homeService.getZonePosition().subscribe(res => {
      this.zonePosition = res.data;
      this.uppercaseZone();
      this.loadZones(this);
    }, err => {
      console.log(err);
    });
  }

  uppercaseZone() {
    for (let i = 0; i < this.zonePosition.length; i++) {
      this.zonePosition[i].name = this.zonePosition[i].name.toUpperCase();
    }
  }

  getInforPerson(id) {
    console.log(id);
    this.detailPersonService.getInfomation(id).subscribe(res => {
      this.infor = res.data;
      this.isLoading = false;
      console.log('infor: ');
      console.log(this.infor);
    }, err => {
      console.log(err);
    });
  }

  getLogPerson(person_id) {
    this.detailPersonService.getLog(person_id).subscribe(res => {
      this.logs = res;
      this.destroyDataTable('#logTable');
      this.chRef.detectChanges();
      $('#logTable').DataTable({
        // 'pageLength': 9,
        // // 'scrollY': '563px'
      });
    }, error => {
      console.log(error);
    });
  }

  ngAfterViewInit() {
  }


  getAllFunction() {
    this.subscription = this.searchProfileService.getFunctions().subscribe(res => {
      this.allFunction = res.data;
      datas = res.data;
      const arr = [];
      $.each(datas, function (i, v) {
        arr.push(datas[i].key);
      });

      drawChart1(arr, this.ip);
    }, error1 => {
      console.log(error1);
    });
  }

  getFunctionSearch(data) {
    this.firstGetPerson = false;
    this.functionsSearch = data;
    this.currentReport.searchresult = this.functionsSearch;
    if (!data) {
      this.getAllFunction();
    } else {
      const arr1 = data.split(',');
      drawChart1(arr1, this.ip);
      this.currentReport.searchOk = arr1;
    }
    let arr = data.split(',');
    if (!data) {
      arr = [];
    }
    console.log(arr);
    this.removeAll();
    this.setWidthHeightCanvas();
    this.loadZones(this);
    this.getPersonsOnMap(arr);


    this.currentReport.searchOk = arr;
    this.currentReport.getAllCurrent(arr);
    this.currentReport.getAllCustomers();
    // this.getAllCustomerHome(arr);
  }

  setTitleTimeZone() {
    this.title = 'Overview: Day';
  }

  setTitleLive() {
    this.homeService.getTimeZone().subscribe(res => {
      this.title = res.data;
      document.getElementById('title-home').innerHTML = res.data;
    }, err => {
      console.log(err);
    });
  }

  // getAllCustomerHome(data) {
  //   this.currentService.getAllCustomer(data).subscribe(res => {
  //   });
  // }

  ngOnChanges() {
  }

  destroyDataTable(obj) {
    $(obj).DataTable().destroy();
  }

  // Send Mail
  getEmail(_email) {
    this.email = _email;
  }

  sendMail(des) {
    $('#modal-mail .close').click();
    this.description = des.value;
    this.mailService.sendMail({
      'email': this.email,
      'description': this.description
    }).subscribe(res => {
      swal('Send Mail Success!', '', 'success');
    }, err => {
      swal('Send Mail Failure!', '', 'error');
    });
    des.value = '';
  }
}
