import {Component, OnInit} from '@angular/core';
import {RuleService} from '../../../../../../services/rules/rule.service';
import {Rule} from '../../../../../../models/rules/rule';
// Data table
import {ChangeDetectorRef} from '@angular/core';
import swal from 'sweetalert';

declare var $: any;
declare var show_table_emoji: any;
declare var show_emoji: any;

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit {
  public rules: any = '';
  public condition: any = '';
  public operatorr: any = '';
  public rule: Rule = new Rule();
  public isShowTilteRules = false;
  public isAdd = true;
  public title: any = 'Rule Informations';
  public idRule: any;

  constructor(public ruleService: RuleService,
              private chRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    // $('.timepickerRuleChat').AnyPicker(
    //   {
    //     mode: 'datetime',
    //
    //     dateTimeFormat: 'HH:mm',
    //
    //     theme: 'Android' // "Default", "iOS", "Android", "Windows"
    //   });
    $('.timepickerRuleChat').datetimepicker(
      {
        format: 'HH:mm',
        sideBySide: true
      });
    this.getRules();
    this.getConditions();
    show_table_emoji();
  }

  getRules() {
    this.ruleService.getRuleChat().subscribe(res => {
      this.rules = res.data;
      show_emoji(res.data);
      this.destroyDataTable('#table-rules');
      this.chRef.detectChanges();
      $('#table-rules').DataTable({
        'pageLength': 100000
      });
    }, err => {
      console.log(err);
    });
  }

  addRules(time_start, time_end) {
    const convert = this.convertString('#content_chat');
    this.rule.additional_condition = '' + this.rule.additional_condition;
    this.rule.content = convert;
    const chat = $('#content_chat').html();
    this.rule.icon_emoji = chat;
    console.log(this.rule);
    console.log(time_start);
    console.log(time_end);
    console.log(convert);
    this.ruleService.addRules(this.rule).subscribe(res => {
      this.rule = new Rule();
      this.getRules();
      $('#content_chat').html('');
      $('#close-create-rule').click();
      swal('Add Rules Success!', '', 'success');
      this.rule = new Rule();
    }, error1 => {
      $('.close').click();
      console.log(error1);
      const pr = error1.statusText;
      swal(pr, 'error');
    });

  }

  updateRules(timeStart, timeEnd) {
    const convert = this.convertString('#content_chat');
    this.rule.additional_condition = '' + this.rule.additional_condition;
    this.rule.content = convert;
    const chat = $('#content_chat').html();
    this.rule.icon_emoji = chat;
    this.rule.time_start = timeStart;
    this.rule.time_end = timeEnd;
    console.log(this.rule);
    console.log(timeStart);
    console.log(timeEnd);
    console.log(convert);
    this.ruleService.updateRule(this.idRule, this.rule).subscribe(res => {
      this.getRules();
      $('#content_chat').html('');
      $('#close-create-rule').click();
      swal('Update Success!', '', 'success');
      this.rule = new Rule();
    }, error1 => {
      $('.close').click();
      console.log(error1);
      const pr = error1.statusText;
      swal(pr, 'error');
    });

  }

  // convert chat to back end
  convertString(obj) {
    const chat = $(obj).html();
    let result = '';
    result = chat.replace(/<p>/g, '').replace(/<[/]p>/g, '').replace(/img class="icon_emoji" src="data:image[/]gif;base64,R0lGODlhAQABAIAAAAAAAP[/][/][/]yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id="icon_emoji/g, 'sprite index=').replace(/">/g, '>').replace(/font color/g, 'color').replace(/[/]font/g, '/color').replace(/&nbsp;/g, ' ').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&').replace(/<div>|<[/]div>/g, '');
    return result;
  }

  setUpdate(id) {
    this.isAdd = false;
    this.title = 'Update Rule Information';
    this.idRule = id;
    this.getRuleById();
    // $('#content_chat').text = this.rule.content;
  }

  getRuleById() {
    this.ruleService.getRuleById(this.idRule).subscribe(res => {
      console.log(res.data);
      this.rule = res.data;
      $('#content_chat').html(res.data.icon_emoji);
      // show_emoji(res.data.icon_emoji);
      if (res.data.type === 6) {
        this.isShowTilteRules = true;
      } else {
        this.isShowTilteRules = false;
      }
    }, err => {
      console.log(err);
    });
  }

  setAddRule() {
    this.isAdd = true;
    this.title = 'Rule Information';
  }

  deleteRule(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Rule!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.ruleService.deleteRule(id).subscribe(res => {
            this.getRules();
            swal('Delete Success!', '', 'success');
          }, error1 => {
            console.log(error1);
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });
  }

  getConditions() {
    this.getOperator();
    this.ruleService.getCondition().subscribe(res => {
      this.condition = res.data;
    }, err => {
      console.log(err);
    });
  }

  getOperator() {
    this.ruleService.getOperator().subscribe(res => {
      this.operatorr = res.data;
    }, error => {
      console.log(error);
    });
  }

  destroyDataTable(obj) {
    $(obj).DataTable().destroy();
  }

  checkType(value) {
    if (value === '1') {
      this.isShowTilteRules = false;
    } else {
      this.isShowTilteRules = true;
    }
  }
}
