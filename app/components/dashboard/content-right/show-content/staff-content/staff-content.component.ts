import {Component, OnInit, OnDestroy, ViewChild, AfterViewInit} from '@angular/core';
import './../../../../../../assets/js/staff.js';
import {Staff} from '../../../../../models/staff/staff';
import {Subject, Subscription} from 'rxjs';
import {StaffService} from '../../../../../services/staff/staff.service';
import {SearchProfileService} from '../../../../../services/searchProfile/search-profile.service';
import {DetailPersonService} from '../../../../../services/detailPerson/detail-person.service';
import {MailService} from '../../../../../services/mail/mail.service';
// Data table
import {ChangeDetectorRef} from '@angular/core';
import swal from 'sweetalert';
import {ConfigIpService} from '../../../../../services/configIP/config-ip.service';


declare var ckEditor: any;
declare var datas: any;
declare var $: any;
declare var openNav: any;
declare var clickItem: any;

@Component({
  selector: 'app-staff-content',
  templateUrl: './staff-content.component.html',
  styleUrls: ['./staff-content.component.css']
})
export class StaffContentComponent implements OnInit, OnDestroy, AfterViewInit {

  public allFunction: any;
  public listCustomer: Staff[] = [];
  public subscription: Subscription;
  public functionsSearch: any = [];
  public functionsSearchStr: any = '';
  public title = 'Staff Management';
  public infor: any = '';
  public logs: any = '';
  // Send Mail
  public email: any = '';
  public description: any = '';
  // is loading staff
  public isLoadingStaff: any = true;
  // Data table
  public currentPage: any = 1;
  public totalPage: any = '';
  public startPage: any = 0;
  public ip: any = '';
// is  loading
  public isloading: any = true;
  constructor(public staffService: StaffService,
              public searchProfileService: SearchProfileService,
              public detailPersonService: DetailPersonService,
              private mailService: MailService,
              private chRef: ChangeDetectorRef,
              private config: ConfigIpService) {
    this.ip = config.getIp();
  }

  ngOnInit() {
    clickItem();
    // CK editor
    ckEditor('editor1');
    // search
    $('#multi-select').dropdown();
    // get data
    this.getAllFunction();
    // Data table
    this.getAllCustomer();
  }

  getInforPersonStaff(id) {
    this.detailPersonService.getInfomation(id).subscribe(res => {
      console.log('infor variable ');
      this.infor = res.data;
      console.log(this.infor);
    }, err => {
      console.log(err);
    });


  }

  getLogPersonStaff(id) {
    this.destroyDataTable('#logTable');
    this.detailPersonService.getLog(id).subscribe(res => {
      this.logs = res;
      this.chRef.detectChanges();
      $('#logTable').DataTable({
        // 'pageLength': 9,
        // // 'scrollY': '563px'
      });
    }, error => {
      console.log(error);
    });
  }

  getAllFunction() {
    this.searchProfileService.getFunctions().subscribe(res => {
      this.allFunction = res.data;
      datas = this.allFunction;
    }, error1 => {
      console.log(error1);
    });
  }

  getFunctionSearch(data) {
    this.functionsSearchStr = data;
    let arr = data.split(',');
    if (!data) {
      arr = [];
    }
    this.functionsSearch = arr;
    this.getAllCustomer();
  }

  ShowDetail() {
    let ids;
    ids = $('#hidId').val();
    openNav('#member-detail');
    this.getInforPersonStaff(ids);
  }


  getAllCustomer() {
    this.isLoadingStaff = true;
    const that = this;
    this.destroyDataTable('#table');
    // setTimeout(function () {
    //   that.subscription = that.staffService.getAllCustomer(that.functionsSearch).subscribe(res => {
    //     that.isLoadingStaff = false;
    //     that.destroyDataTable('#table');
    //     that.listCustomer = res.data;
    //     console.log(that.listCustomer);
    //     that.dataTablesConfig('#table', '#navigation_customer');
    //   }, error1 => {
    //     console.log('Loi nhe');
    //   });
    //  }, 100);
    // console.log(data);
    // const token = localStorage.getItem('currentUser');
    // console.log(token)
    $('#table').DataTable({
      processing: true,
      'pageLength': 8,
      'serverSide': true,
      ajax: {
        url: that.ip + '/api/customer/anyData_post?function=' + that.functionsSearchStr,
      },
      columns: [
        {data: 'name', name: 'name'},
        {data: 'join_date', name: 'join_date'},
        {data: 'title', name: 'title'},
        {data: 'office_location', name: 'office_location'},
        {data: 'title', name: 'title'},
      ],
      columnDefs: [
        {
          'targets': 0,
          'render': function (data, type, row) {
            return `<div class="line h-100" style="background-color:` + row.color + ` "></div>` +
              `<div class="rounded-circle dot" style="background-color:` + row.gender_color + ` "></div>` +
              `<img src="` + row.image[0].file + `"class="rounded-circle img customerClick" data-id="` + row.id + `" style="border: 2px solid ` + row.color + `" onclick="closeNav('#list-profile')" style="cursor: pointer">` +
              `<div style="display: inline-block; margin-left: 10px; margin-top: -18px;"  onclick="closeNav('#list-profile'),ShowDetail(` + row.id + `)" class="customerClick" data-id="` + row.id + `">
                    <span>` + row.name + `</span>
                    <span style="color: #c5c0c0; display: block">` + row.department + ` / ` + row.person_id + ` </span>
                </div>`;
          }
        },
        {
          'targets': 5,
          'render': function (data, type, row) {
            return `<div class="rounded-circle mail sendMail" data-toggle="modal" data-target="#modal-mail"  data-email="` + row.email + `">
                <img style="margin-top: 10px;border: none; cursor: pointer" src="./assets/images/icon/ico_mail_black.png">
            </div>`;
          }
        }
      ]
    });
    setTimeout(function () {
      that.setPagiInfo('#table', '#navigation_customer');
      $(document).on('click', '.sendMail', function () {
        const email = $(this).data('email');
        that.getEmail(email);
      });
    }, 5000);
    this.actionPagiPage('#table', '#navigation_customer');
  }


// get email
  getEmail(_email) {
    console.log(_email);
    this.email = _email;
  }

  ngAfterViewInit() {
  }

// get description
  sendMail(des) {
    $('#modal-mail .close').click();
    this.description = des.value;
    this.mailService.sendMail({
      'email': this.email,
      'description': this.description
    }).subscribe(res => {
      $('#editor1').val('');
      swal('Send Mail Success!', '', 'success');
    }, err => {
      swal('Send Mail Failure!', '', 'error');
    });

  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();
  }

  destroyDataTable(obj) {
    $(obj).DataTable().destroy();
  }


  setPagiInfo(objTable, objPagi) {
    const table = $(objTable).DataTable();
    const currentPage = table.page.info().page + 1;
    const totalPage = table.page.info().pages;
    const pagi = 'Page ' + currentPage + ' of ' + totalPage;
    $(objPagi + ' .pagin').html(pagi);
  }

  actionPagiPage(objTable, objPagi) {
    const that = this;
    $(objTable + ' th').on('click', function () {
      that.setPagiInfo(objTable, objPagi);
    });
    // action pagination page
    const table = $(objTable).DataTable();
    $(objPagi + ' .ic_next').on('click', function () {
      table.page('next').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
    $(objPagi + ' .ic_prev').on('click', function () {
      table.page('previous').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
  }

}
