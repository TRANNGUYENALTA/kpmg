import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FeedsService} from '../../../../../services/feeds/feeds.service';
import './../../../../../../assets/js/feed.js';
import {Feed, FeedInfor, RuleFeedInformation} from '../../../../../models/feed/feed';
import {ConfigIpService} from '../../../../../services/configIP/config-ip.service';
import {RuleService} from '../../../../../services/rules/rule.service';

// Data Table
import {ChangeDetectorRef} from '@angular/core';
import swal from 'sweetalert';

declare var $: any;
declare var setHeightElement: any;
declare var feedFunction: any;
declare var monthClick: any;
declare var getselect2: any;
declare var setTag: any;
declare var checkImageFeed: any;
declare var checkImageFeedInfo: any;
declare var isImageFeed: any;
declare var isImageFeedInfo: any;
declare var css: any;
declare var show_icon: any;
declare var show_anypicker: any;


declare var removeclass: any;
declare var show_table_emojiAddfeed: any;


@Component({
  selector: 'app-feeds-content',
  templateUrl: './feeds-content.component.html',
  styleUrls: ['./feeds-content.component.css']
})

export class FeedsContentComponent implements OnInit, OnDestroy, AfterViewInit {
  public subcription1: Subscription;
  public subcription2: Subscription;
  public dataMonth: any = '';
  public feed: Feed = new Feed();
  public updateFeed: Feed = new Feed();
  public feedInfor: FeedInfor = new FeedInfor();
  public feedInforUpdate: FeedInfor = new FeedInfor();
  public idFeedInfo: any;
  public oneFeedInfo: any = '';
  public feedById: any = '';
  public input = new FormData();
  public inputUpdate = new FormData();
  public inputFeedInfo = new FormData();
  public inputFeedRule = new FormData();
  public inputFeedInfoUpdate = new FormData();
  public isUpdate: any = true;
  public idUpdate: any;
  public isFirstCheckDate: any = true;
  // Config domain
  public IP: any = '';

  // Feed Information
  public allFeedInfo: any = '';
  // check image
  public isFeedImage: any = true;
  public isFeedInfoImage: any = true;

  // Feed Information Rules
  public condition: any = '';
  public operatorr: any = '';
  public feedRules: any = '';
  public isAddImage: any = true;
  public showImage: any = 'd-block';
  public showIcon: any = 'd-none';
  public feedParentId: any;
  public ruleId: any;
  public isAddRuleAction: any;
  public titleRuleAction: any = 'Add Rule Feed Information';
  public feedInfoRule: RuleFeedInformation = new RuleFeedInformation();

  // isEvent
  public isActiveEvent: any = false;
  // Loading
  public isLoadingCalendar: any = true;

  constructor(public feedService: FeedsService,
              private chRef: ChangeDetectorRef,
              private config: ConfigIpService,
              private ruleService: RuleService) {
    this.IP = config.getIp();
  }
  ngAfterViewInit() {
    const that = this;
    // Active button event
    $(document).on('click', '#button_event', function () {
      that.eventAction();
    });
  }
  ngOnInit() {
    const that = this;
    setHeightElement('#external');
    removeclass();
    show_anypicker();
    show_table_emojiAddfeed('addFeed');
    show_table_emojiAddfeed('updateFeed');
    // check image Feed
    checkImageFeed('#margin');
    checkImageFeed('#margin_update');

    // check image Feed Information
    checkImageFeedInfo('#file_infor_feed');
    checkImageFeedInfo('#file_infor_feed_update');
    checkImageFeedInfo('#blah0_feedInfoRule');

    $(document).on('click', '.remove_event', function () {
      that.isUpdate = false;
      const id = $(this).data('id');
      that.deleteEvent(id);
    });
    $(document).on('click', '.remove_item', function () {
      that.isUpdate = false;
    });

    $(document).on('click', '.content_event', function () {
      if (that.isUpdate) {
        const id = $(this).children('.remove_event').data('id');
        that.idUpdate = id;
        that.getFeedById(id);
        $('#modal_updateFeed').click();
      }
      that.isUpdate = true;
    });
    $(document).on('click', '.item_list', function () {
      if (that.isUpdate) {
        console.log(that.idUpdate);
        that.getFeedById(that.idUpdate);
        $('#modal_updateFeed').click();
      }
      that.isUpdate = true;
    });
    this.getAllFeed();
    this.getFeedsMonth();
    this.getFeedInfomation();
    // Rules Feed Information
    this.getOperator();
    this.getConditions();
    getselect2(this.IP);

  }

  eventAction() {
    let event;
    this.isActiveEvent = !this.isActiveEvent;
    if (this.isActiveEvent) {
      $('#button_event').addClass('event_active');
    } else {
      $('#button_event').removeClass('event_active');
    }
    if (this.isActiveEvent) {
      event = {'event': true};

    } else {
      event = {'event': false};
    }
    this.feedService.eventSocket(event);
  }

  setAddRuleFeed(file, timeStart, timeEnd) {
    this.changeOptionTrue();
    this.isAddRuleAction = true;
    this.titleRuleAction = 'Add Rule Feed Information';
    // Reset field
    this.feedInfoRule = new RuleFeedInformation();
    $('#blah0_feedInfoRule').val('');
    timeStart.value = '';
    timeEnd.value = '';
    this.inputFeedRule = new FormData();
    $('.blah0_feedInfoRule').attr('src', '').removeAttr('style');
    $('.blah0_feedInfoRule_plus_img').css('display', 'inline-block');
    $('.blah0_feedInfoRule_add_img').css('display', 'inline-block');
  }

  // Rule feed information
  addNewRule(file, timeStart, timeEnd) {
    console.log('Add New rule working');
    this.feedInfoRule.feed_id = this.feedParentId;
    console.log(timeStart);
    console.log(timeEnd);
    console.log(this.feedInfoRule);
    console.log(this.isAddImage);

    if (this.isAddImage) {
      if (!file.files[0]) {
        this.inputFeedRule.append('file', null);
      } else {
        this.inputFeedRule.append('file', file.files[0]);
        console.log(file.files[0]);
      }

    } else {
      this.inputFeedRule.append('file', this.feedInfoRule.file);
      console.log(this.feedInfoRule.file);
    }

    this.inputFeedRule.append('feed_id', this.feedInfoRule.feed_id);
    this.inputFeedRule.append('element', this.feedInfoRule.element);
    this.inputFeedRule.append('prerequisite', this.feedInfoRule.prerequisite);
    this.inputFeedRule.append('value', this.feedInfoRule.value);
    this.inputFeedRule.append('type', this.feedInfoRule.type);
    this.inputFeedRule.append('title', this.feedInfoRule.title);
    this.inputFeedRule.append('content', this.feedInfoRule.content);
    this.inputFeedRule.append('time_start', timeStart.value);
    this.inputFeedRule.append('time_end', timeEnd.value);
    this.feedService.addRuleFeedInformation(this.inputFeedRule).subscribe(res => {
      console.log(res);
      swal('Add Rule  Success!', '', 'success');
      this.getRulesFeed(this.feedParentId);
      // Reset field
      this.feedInfoRule = new RuleFeedInformation();
      $('#blah0_feedInfoRule').val('');
      timeStart.value = '';
      timeEnd.value = '';
      this.inputFeedRule = new FormData();
      $('.blah0_feedInfoRule').attr('src', '').removeAttr('style');
      $('.blah0_feedInfoRule_plus_img').css('display', 'inline-block');
      $('.blah0_feedInfoRule_add_img').css('display', 'inline-block');
    }, err => {
      console.log(err);
      swal('Add Rule Failure!', '', 'error');
    });
  }

  // Rule feed information
  updateRuleFeed(file, timeStart, timeEnd) {
    console.log('update New rule working');
    this.feedInfoRule.feed_id = this.feedParentId;
    console.log(timeStart);
    console.log(timeEnd);
    console.log(this.feedInfoRule);
    console.log(this.isAddImage);

    if (this.isAddImage) {
      if (!file.files[0]) {
        this.inputFeedRule.append('file', null);
      } else {
        this.inputFeedRule.append('file', file.files[0]);
        console.log(file.files[0]);
      }

    } else {
      this.inputFeedRule.append('file', this.feedInfoRule.file);
      console.log(this.feedInfoRule.file);
    }

    this.inputFeedRule.append('feed_id', this.feedInfoRule.feed_id);
    this.inputFeedRule.append('element', this.feedInfoRule.element);
    this.inputFeedRule.append('prerequisite', this.feedInfoRule.prerequisite);
    this.inputFeedRule.append('value', this.feedInfoRule.value);
    this.inputFeedRule.append('type', this.feedInfoRule.type);
    this.inputFeedRule.append('title', this.feedInfoRule.title);
    this.inputFeedRule.append('content', this.feedInfoRule.content);
    this.inputFeedRule.append('time_start', timeStart.value);
    this.inputFeedRule.append('time_end', timeEnd.value);

    this.feedService.updateRuleFeed(this.ruleId, this.inputFeedRule).subscribe(res => {
      swal('Update Rule  Success!', '', 'success');
      this.getRulesFeed(this.feedParentId);
      // Reset field
      this.feedInfoRule = new RuleFeedInformation();
      $('#blah0_feedInfoRule').val('');
      timeStart.value = '';
      timeEnd.value = '';
      this.inputFeedRule = new FormData();
      $('.blah0_feedInfoRule').attr('src', '').removeAttr('style');
      $('.blah0_feedInfoRule_plus_img').css('display', 'inline-block');
      $('.blah0_feedInfoRule_add_img').css('display', 'inline-block');
    }, err => {
      console.log(err);
      swal('Update Rule Failure!', '', 'error');
    });
  }

  deleteFeedRule(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Rule!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.feedService.deleteFeedRule(id).subscribe(res => {
            swal('Delete Success!', '', 'success');
            this.getRulesFeed(this.feedParentId);
          }, error1 => {
            console.log(error1);
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });

  }

  getDetailRule(id) {
    console.log(id);
    this.ruleId = id;
    this.isAddRuleAction = false;
    this.titleRuleAction = 'Update Rule Feed Information';
    this.feedService.getDetailRule(id).subscribe(res => {
      this.setUpdateRuleInfomation(res.data);
    }, err => {
      console.log(err);
    });
  }

  setUpdateRuleInfomation(data) {
    $('.blah0_feedInfoRule').attr('src', '').removeAttr('style');
    $('.blah0_feedInfoRule_plus_img').css('display', 'inline-block');
    $('.blah0_feedInfoRule_add_img').css('display', 'inline-block');
    this.feedInfoRule.file = '';
    this.feedInfoRule.type = data.type;
    this.feedInfoRule.title = data.title;
    this.feedInfoRule.content = data.content;
    this.feedInfoRule.element = data.element;
    this.feedInfoRule.prerequisite = data.prerequisite;
    this.feedInfoRule.value = data.value;
    this.feedInfoRule.time_start = data.time_start;
    this.feedInfoRule.time_end = data.time_end;
    this.isAddImage = true;
    if (data.file.icon) {
      this.showIcon = 'd-block';
      this.showImage = 'd-none';
      this.isAddImage = false;
      this.feedInfoRule.file = data.file.icon;
    } else {
      if (data.file.image) {
        this.isAddImage = true;
        this.showImage = 'd-block';
        this.showIcon = 'd-none';
        $('.blah0_feedInfoRule_plus_img').css('display', 'none');
        $('.blah0_feedInfoRule_add_img').css('display', 'none');
        $('#blah0_feedInfoRule_img').attr('src', data.file.image);
      } else {
        this.isAddImage = true;
        $('.blah0_feedInfoRule_plus_img').css('display', 'none');
        $('.blah0_feedInfoRule_add_img').css('display', 'none');
        $('#blah0_feedInfoRule_img').attr('src', data.file);
      }
    }
  }


  // get condition and operator
  getConditions() {
    this.getOperator();
    this.ruleService.getCondition().subscribe(res => {
      this.condition = res.data;
    }, err => {
      console.log(err);
    });
  }


  getOperator() {
    this.ruleService.getOperator().subscribe(res => {
      this.operatorr = res.data;
    }, error => {
      console.log(error);
    });
  }

  getIdEventMonth(id) {
    this.idUpdate = id;
  }

  getAllFeed() {
    const that = this;
    // get All Data
    this.subcription1 = this.feedService.getAllFeed().subscribe(res => {
      this.isLoadingCalendar = false;
      feedFunction(res.data);
      show_icon(res.data);
    }, error1 => {
      console.log(error1);
    });
  }

  getFeedsMonth() {
    const datas = [];
    this.subcription2 = this.feedService.getFeedsWithMonth().subscribe(res => {
      $.each(res.data, function (index, item) {
        datas.push({
          title: index,
          event: item
        });
      });
      this.dataMonth = datas;
      console.log(this.dataMonth);
    }, error1 => {
      console.log(error1);
    });
  }

// Add Feed
  addFeed(file, dateStart, dateEnd, timeStart, timeEnd, favorite) {
    this.feed.date_start = dateStart.value;
    this.feed.date_end = dateEnd.value;
    this.feed.time_start = timeStart.value;
    this.feed.time_end = timeEnd.value;
    this.feed.favorite = $('#favorite_feed').val();
    this.feed.feed_emoji = $('#content_chat_addFeed').html();

    this.feed.description = this.convertString('#content_chat_addFeed');
    console.log('convert: ');
    console.log(this.feed.description);
    console.log('html');
    console.log(this.feed.feed_emoji);
    console.log(file.files[0]);
    console.log(this.feed.date_start);
    console.log(this.feed.date_end);
    console.log(this.feed.time_start);
    console.log(this.feed.time_end);
    console.log(this.feed.title);
    console.log(this.feed.time_repeat);
    console.log(this.feed.favorite);
    console.log(this.feed.feed_emoji);
    if (!file.files[0]) {
      this.input.append('file', null);
    } else {
      this.input.append('file', file.files[0]);
    }
    this.input.append('title', this.feed.title);
    this.input.append('description', this.feed.description);
    this.input.append('date_start', this.feed.date_start);
    this.input.append('date_end', this.feed.date_end);
    this.input.append('time_start', this.feed.time_start);
    this.input.append('time_end', this.feed.time_end);
    this.input.append('favorite', this.feed.favorite);
    this.input.append('time_repeat', this.feed.time_repeat);
    this.input.append('feed_emoji', this.feed.feed_emoji);
    //
    this.feedService.addFeed(this.input).subscribe(res => {
      this.getFeedsMonth();
      this.getAllFeed();
      $('.close').click();
      swal('Add Success!', '', 'success');
      this.input = new FormData();
      this.feed = new Feed();
      dateStart.value = '';
      dateEnd.value = '';
      timeStart.value = '';
      timeEnd.value = '';
      $('#margin').val('');
      $('#favorite_feed').val('');
      $('#favorite_feed').select2();
      $('.blah0_addFeed').attr('src', '').removeAttr('style');
      $('.blah0_addFeed_plus_img').css('display', 'inline-block');
      $('#btn_addimg').css('display', 'block');
      $('#content_chat_addFeed').html('');
      $('#add_img').css('display', 'inline-block');
      css();

    }, error => {
      this.getFeedsMonth();
      this.getAllFeed();
      console.log(error);
      swal('All field must have value!', '', 'error');

    });

  }

  // convert chat to back end
  convertString(obj) {
    const chat = $(obj).html();
    let result = '';
    result = chat.replace(/<p>/g, '').replace(/<[/]p>/g, '')
      .replace(/img class="icon_emoji" src="data:image[/]gif;base64,R0lGODlhAQABAIAAAAAAAP[/][/][/]yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id="icon_emojiaddFeed/g, 'sprite index=')
      .replace(/">/g, '>').replace(/font color/g, 'color').replace(/[/]font/g, '/color').replace(/&nbsp;/g, ' ').replace(/<div>|<[/]div>/g, '')
      .replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
    return result;
  }

  convertStringUpdate(obj) {
    const chat = $(obj).html();
    let result = '';
    result = chat.replace(/<p>/g, '').replace(/<[/]p>/g, '')
      .replace(/img class="icon_emoji" src="data:image[/]gif;base64,R0lGODlhAQABAIAAAAAAAP[/][/][/]yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id="icon_emojiupdateFeed/g, 'sprite index=')
      .replace(/">/g, '>').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&').replace(/<div>|<[/]div>/g, '')
      .replace(/font color/g, 'color').replace(/[/]font/g, '/color').replace(/&nbsp;/g, ' ')
      .replace(/img class="icon_emoji" src="data:image[/]gif;base64,R0lGODlhAQABAIAAAAAAAP[/][/][/]yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" id="icon_emojiaddFeed/g, 'sprite index=');
    return result;
  }

  // check image width height
  checkImageFeed() {
    const that = this;
    setTimeout(function () {
      // set value is checked image
      that.isFeedImage = isImageFeed;
      console.log(that.isFeedImage);
    }, 20);
  }

  // check image Feed Info width height
  checkImageFeedInfo() {
    const that = this;
    setTimeout(function () {
      // set value is checked image
      that.isFeedInfoImage = isImageFeedInfo;
      console.log(that.isFeedInfoImage);
    }, 20);
  }

  getRecurrence(value) {
    this.feed.time_repeat = value;
    this.updateFeed.time_repeat = value;
    console.log(value);
  }

  deleteEvent(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Event!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.feedService.deleteEvent(id).subscribe(res => {
            this.getFeedsMonth();
            $('#calendar').fullCalendar('destroy');
            this.getAllFeed();
            monthClick();
            swal('Delete Success!', '', 'success');
          }, error1 => {
            console.log(error1);
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });
  }

// Update Feed
  getFeedById(id) {
    this.feedService.getFeedById(id).subscribe(res => {
      this.feedById = res.data;
      this.feed.time_repeat = this.feedById.time_repeat;
      console.log(this.feedById);
      this.setDataFieldInputUpdate();
    }, err => {
      console.log(err);
    });
  }

  // flow data to modal box update feed
  setDataFieldInputUpdate() {
    css();
    const recurrent = this.feedById.time_repeat;
    const objClick = '.recurrence-' + recurrent;
    $(objClick).click();
    $('.blah0_updateFeed_plus_img').css('display', 'none');
    $('.blah0_updateFeed_add_img').css('display', 'none');
    $('#img_file').attr('src', this.feedById.file);
    $('#title_update').val(this.feedById.title);
    $('#content_chat_updateFeed').html(this.feedById.feed_emoji);
    this.updateFeed.date_start = this.feedById.date_start;
    this.updateFeed.date_end = this.feedById.date_end;
    this.updateFeed.time_start = this.feedById.time_start;
    this.updateFeed.time_end = this.feedById.time_end;
    setTag(this.idUpdate, this.IP);
  }

  // update Feed Action
  updateFeedAction(file, date_start, date_end, time_start, time_end) {
    console.log(this.idUpdate);
    console.log(this.feedById);
    console.log(this.checkDateEvent(date_start.value, date_end.value));
    this.updateFeed.feed_emoji = $('#content_chat_updateFeed').html();
    this.updateFeed.description = this.convertStringUpdate('#content_chat_updateFeed');
    if (file.files[0]) {
      this.updateFeed.file = file.files[0];
    }
    if (!this.updateFeed.title) {
      this.updateFeed.title = this.feedById.title;
    }
    if (!this.updateFeed.description) {
      this.updateFeed.description = this.feedById.description;
    }
    this.updateFeed.favorite = $('#favorite_feedUpdate').val().toString();

    if (!date_start) {
      this.inputUpdate.append('date_start', null);
    } else {
      this.inputUpdate.append('date_start', date_start.value);
    }

    if (!date_end) {
      this.inputUpdate.append('date_end', null);
    } else {
      this.inputUpdate.append('date_end', date_end.value);
    }

    if (!time_start) {
      this.inputUpdate.append('time_start', null);
    } else {
      this.inputUpdate.append('time_start', time_start.value);

    }
    if (!time_end) {
      this.inputUpdate.append('time_end', null);
    } else {
      this.inputUpdate.append('time_end', time_end.value);
    }

    // console.log('id: ');
    // console.log(this.idUpdate);
    // console.log('file : ');
    // console.log(this.updateFeed.file);
    // console.log('title : ');
    // console.log(this.updateFeed.title);
    // console.log('descritiom : ');
    // console.log(this.updateFeed.description);
    // console.log('emoiji : ');
    // console.log(this.updateFeed.feed_emoji);
    // console.log('date start : ');
    // console.log(date_start.value);
    // console.log('date end : ');
    // console.log(date_end.value);
    // console.log('time start : ');
    // console.log(time_start.value);
    // console.log('date end : ');
    // console.log(time_end.value);
    // console.log('Time Repeat : ');
    // console.log(this.updateFeed.time_repeat);
    // console.log('Preference Tags : ');
    // console.log(this.updateFeed.favorite);

    this.inputUpdate.append('title', this.updateFeed.title);
    this.inputUpdate.append('description', this.updateFeed.description);
    this.inputUpdate.append('favorite', this.updateFeed.favorite);
    this.inputUpdate.append('file', this.updateFeed.file);
    this.inputUpdate.append('time_repeat', this.updateFeed.time_repeat);
    this.inputUpdate.append('feed_emoji', this.updateFeed.feed_emoji);

    this.feedService.updateFeed(this.idUpdate, this.inputUpdate).subscribe(res => {
      $('.close').click();
      swal('Update Success!', '', 'success');
      $('#calendar').fullCalendar('destroy');
      this.getAllFeed();
      this.getFeedsMonth();
      $('.fc-view-container').css('display', 'block');
      $('.card_list').css('display', 'none');
    }, error => {
      console.log(error);
      swal('Update Failure!!', '', 'error');
    });
    // reset fields
    $('#margin_update').val('');
    this.inputUpdate = new FormData();
    this.updateFeed = new Feed();
    date_start.value = '';
    date_end.value = '';

  }

  checkDateEvent(date_start, date_end) {
    if (!date_start && !date_end) {
      return true;
    }
    if (!date_start || !date_end) {
      return false;
    }
    if (date_start > date_end) {
      return false;
    } else {
      return true;
    }
  }

  ngOnDestroy() {
    this.subcription1.unsubscribe();
    this.subcription2.unsubscribe();
  }

  // Add Feed Information
  addFeedInformation(dateStart, dateEnd, file) {
    this.inputFeedInfo.append('name', this.feedInfor.name);
    this.inputFeedInfo.append('url', this.feedInfor.url);
    this.inputFeedInfo.append('file', file.files[0]);
    this.inputFeedInfo.append('description', this.feedInfor.description);
    this.inputFeedInfo.append('time_start', dateStart.value);
    this.inputFeedInfo.append('time_end', dateEnd.value);
    this.inputFeedInfo.append('time_repeat', this.feedInfor.time_repeat);
    this.feedService.addFeedInformation(this.inputFeedInfo).subscribe(res => {

      this.getFeedInfomation();
      $('#close_addFeedInfo').click();
      // reset field
      dateStart.value = '';
      dateEnd.value = '';
      $('.blah0_addFeedInfo').attr('src', '').removeAttr('style');
      $('#plus_imgex').css('display', 'inline-block');
      $('#btn_addimg_feedInfo').css('display', 'block');
      this.feedInfor = new FeedInfor();
      swal('Add Success!', '', 'success');
    }, err => {
      swal('All field must have value!!', '', 'error');
    });
  }


  // Get Feed Information
  getFeedInfomation() {
    this.feedService.getAllFeedInformation().subscribe(res => {
      this.destroyDataTable('#table_feedInfo');
      this.allFeedInfo = res.data;
      // config dataTables
      this.dataTablesConfig('#table_feedInfo', '#navigation_feedInfo');
    }, err => {
      console.log(err);
    });
  }

  deleteFeedInfo(id) {
    swal({
      title: 'Are you sure?',
      text: 'Once deleted, you will not be able to recover this Feed Information!',
      icon: 'warning',
      buttons: [true, true],
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.feedService.deleteFeedInfo(id).subscribe(res => {
            this.getFeedInfomation();
            swal('Delete Success!', '', 'success');
          }, error1 => {
            console.log(error1);
          });
        } else {
          swal('Your imaginary file is safe!');
        }
      });
  }

  getFeedInforById(id) {
    this.idFeedInfo = id;
    this.feedService.getFeedInfoById(id).subscribe(res => {
      this.oneFeedInfo = res.data;
      console.log(this.oneFeedInfo);
      this.setFeedInforUpdate();
    }, err => {
      console.log(err);
    });
  }

  setFeedInforUpdate() {
    $('.blah0_updateFeedInfo_plus_img').css('display', 'none');
    $('#add_img_feed_info_update').css('display', 'none');
    $('#img_file_info_update').attr('src', this.oneFeedInfo.file);
    $('#title_update').val(this.oneFeedInfo.title);
    $('#description_update').val(this.oneFeedInfo.description);
    this.feedInforUpdate.name = this.oneFeedInfo.name;
    this.feedInforUpdate.url = this.oneFeedInfo.url;
    this.feedInforUpdate.description = this.oneFeedInfo.description;
    this.feedInforUpdate.time_repeat = this.oneFeedInfo.time_repeat;
    this.feedInforUpdate.date_start = this.oneFeedInfo.time_start;
    this.feedInforUpdate.date_end = this.oneFeedInfo.time_end;
  }

// update Feed Information
  updateFeedInfo(dateStart, dateEnd, file) {
    console.log(file.files[0]);
    if (file.files[0]) {
      console.log('change');
      this.inputFeedInfoUpdate.append('file', file.files[0]);
    } else {
      console.log('no change');
      console.log(file);
      this.inputFeedInfoUpdate.append('file', null);
    }
    this.inputFeedInfoUpdate.append('name', this.feedInforUpdate.name);
    this.inputFeedInfoUpdate.append('url', this.feedInforUpdate.url);
    this.inputFeedInfoUpdate.append('description', this.feedInforUpdate.description);
    this.inputFeedInfoUpdate.append('time_start', dateStart.value);
    this.inputFeedInfoUpdate.append('time_end', dateEnd.value);
    this.inputFeedInfoUpdate.append('time_repeat', this.feedInforUpdate.time_repeat);
    console.log(this.feedInforUpdate.name);
    console.log(this.feedInforUpdate.url);
    console.log(this.feedInforUpdate.description);
    console.log(this.feedInforUpdate.time_repeat);
    console.log(dateStart.value);
    console.log(dateEnd.value);

    this.feedService.updateFeedInfoById(this.idFeedInfo, this.inputFeedInfoUpdate).subscribe(res => {
      this.getFeedInfomation();
      $('#close_addFeedInfoUpdate').click();
      swal('Update Success!', '', 'success');
      this.inputFeedInfoUpdate = new FormData();
      $('.blah0').attr('src', '').removeAttr('style');
      $('#add_img_feed_info_update').css('display', 'block');
      $('#plus_imgex').css('display', 'inline-block');
      $('#file_infor_feed_update').val('');
    }, err => {
      swal('All field must have value!!', '', 'error');
    });
  }

  // Rules Feed
  getRulesFeed(id) {
    this.feedParentId = id;
    this.feedService.getRuleFeedInfo(id).subscribe(res => {
      this.feedRules = res.data;
      this.destroyDataTable('#table-rules-feedInfo');
      console.log(this.feedRules);
      this.chRef.detectChanges();
      $('#table-rules-feedInfo').DataTable({
        'pageLength': 100000,
      });
    }, err => {
      console.log(err);
    });
  }

// change Option
  changeOptionTrue() {
    this.isAddImage = true;
    this.showImage = 'd-block';
    this.showIcon = 'd-none';
    this.feedInfoRule.type = '5';
  }

  changeOptionFalse() {
    this.isAddImage = false;
    this.showImage = 'd-none';
    this.showIcon = 'd-block';
    this.feedInfoRule.type = '3';
  }

  // dataTable function
  dataTablesConfig(objTable, objPagi) {
    // You'll have to wait that changeDetection occurs and projects data into
    // the HTML template, you can ask Angular to that for you ;-)
    this.chRef.detectChanges();
    $(objTable).DataTable({
      'pageLength': 10
    });
    this.setPagiInfo(objTable, objPagi);
    this.actionPagiPage(objTable, objPagi);
  }

  destroyDataTable(obj) {
    $(obj).DataTable().destroy();
  }

  setPagiInfo(objTable, objPagi) {
    const table = $(objTable).DataTable();
    const currentPage = table.page.info().page + 1;
    const totalPage = table.page.info().pages;
    const pagi = 'Page ' + currentPage + ' of ' + totalPage;
    $(objPagi + ' .pagin').html(pagi);
  }

  actionPagiPage(objTable, objPagi) {
    const that = this;
    $(objTable + ' th').on('click', function () {
      that.setPagiInfo(objTable, objPagi);
    });
    // action pagination page
    const table = $(objTable).DataTable();
    $(objPagi + ' .ic_next').on('click', function () {
      table.page('next').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
    $(objPagi + ' .ic_prev').on('click', function () {
      table.page('previous').draw('page');
      that.setPagiInfo(objTable, objPagi);
    });
  }
}
