import {Component, OnInit} from '@angular/core';

declare var setNavLeftIcon: any;
declare var $: any;
declare var toggleMenu: any;
declare var setIconActiveNavLink: any;
declare var setMouseoverMenuNav: any;
declare var setMouseOutMenuNav: any;

@Component({
  selector: 'app-navbar-left',
  templateUrl: './navbar-left.component.html',
  styleUrls: ['./navbar-left.component.css']
})
export class NavbarLeftComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    // toggle menu
    $('.toggle-button').on('click', function () {
      toggleMenu();
    });
    setNavLeftIcon();
    $(document).on('click', '.name_brand', function () {
      $('.dashboard').click();
    });
  }

  resetMenu(obj) {
    setTimeout(function () {
      $(obj).addClass('active');
      setIconActiveNavLink();
    }, 0.01);
  }
}
