export class Feed {
  public title: string;
  public description: string;
  public file: any;
  public date_start: any;
  public date_end: any;
  public time_start: any;
  public time_end: any;
  public favorite: string;
  public time_repeat: any;
  public feed_emoji: any;

  constructor() {
    this.title = '';
    this.description = '';
    this.file = null;
    this.favorite = '1,2';
    this.time_repeat = null;
    this.feed_emoji = '';
  }
}
export class RuleFeedInformation {
  public feed_id: any;
  public type: string;
  public element: string;
  public prerequisite: string;
  public value: string;
  public title: string;
  public time_start: string;
  public time_end: string;
  public content: string;
  public file: any;

  constructor() {
    this.type = '5';
    this.element = '';
    this.prerequisite = '';
    this.value = '';
    this.title = '';
    this.time_start = '00:00';
    this.time_end = '23:59';
    this.content = '';
  }
}

export class FeedInfor {
  public name: string;
  public url: string;
  public file: any;
  public description: string;
  public date_start: string;
  public date_end: string;
  public time_repeat: any;

  constructor() {
    this.name = '';
    this.url = '';
    this.file = null;
    this.description = '';
    this.time_repeat = null;
  }

}
