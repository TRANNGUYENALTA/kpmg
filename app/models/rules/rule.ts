export class Rule {
  public type: string;
  public main_condition: string;
  public prerequisite: string;
  public additional_condition: string;
  public title: string;
  public time_start: string;
  public time_end: string;
  public content: string;
  public external: string;
  public icon_emoji: string;

  constructor() {
    this.type = '';
    this.main_condition = '';
    this.prerequisite = '';
    this.additional_condition = '';
    this.title = '';
    this.time_start = '00:00';
    this.time_end = '23:59';
    this.content = '';
    this.external = '';
    this.icon_emoji = '';
  }
}
