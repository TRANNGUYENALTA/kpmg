import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Feed} from '../../models/feed/feed';
import {ConfigIpService} from '../configIP/config-ip.service';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class FeedsService {
  private ip: any = '';
  private urlGetAllFeed = '/api/feed_data/anyData';
  private urlGetFeedMonth = '/api/feed_data/get_month';
  private urlAddFeed = '/api/feed_data';
  private urlGetFeedById = '/api/feed_data/show/';
  private urlUpdateFeedById = '/api/feed_data/';
  // Feed infomation
  private urlGetFeedInfor = '/api/feed/any-data';
  private urlFeedInfor = '/api/feed/store';
  private urlDelFeedInfo = '/api/feed/destroy/';
  private urlGetFeedInfoById = '/api/feed/show/';
  private urlUpdateFeedInfo = '/api/feed/update/';
  // Rules feed information
  private urlGetRulesFeedInfoById = '/api/feed/rule/any-data/';
  private urlAddNewRuleFeed = '/api/feed/rule/store';
  private urlDeleteFeedRule = '/api/feed/rule/destroy/';
  private urlGetDetailRule = '/api/feed/rule/show/';
  private urlUpdateRuleFeed = '/api/feed/rule/update/';
  //  Socket
  private urlSocket = ':6009';


  constructor(private http: HttpClient, private configIp: ConfigIpService) {
    this.ip = configIp.getIp();
    this.urlGetAllFeed = this.ip + this.urlGetAllFeed;
    this.urlGetFeedMonth = this.ip + this.urlGetFeedMonth;
    this.urlAddFeed = this.ip + this.urlAddFeed;
    this.urlGetFeedById = this.ip + this.urlGetFeedById;
    this.urlUpdateFeedById = this.ip + this.urlUpdateFeedById;
    this.urlGetFeedInfor = this.ip + this.urlGetFeedInfor;
    this.urlFeedInfor = this.ip + this.urlFeedInfor;
    this.urlDelFeedInfo = this.ip + this.urlDelFeedInfo;
    this.urlGetFeedInfoById = this.ip + this.urlGetFeedInfoById;
    this.urlUpdateFeedInfo = this.ip + this.urlUpdateFeedInfo;
    this.urlGetRulesFeedInfoById = this.ip + this.urlGetRulesFeedInfoById;
    this.urlAddNewRuleFeed = this.ip + this.urlAddNewRuleFeed;
    this.urlDeleteFeedRule = this.ip + this.urlDeleteFeedRule;
    this.urlGetDetailRule = this.ip + this.urlGetDetailRule;
    this.urlUpdateRuleFeed = this.ip + this.urlUpdateRuleFeed;
    // socket
    this.urlSocket = this.ip + this.urlSocket;

  }

  getAllFeed(): Observable<any> {
    return this.http.get<any>(this.urlGetAllFeed);

  }

  getFeedsWithMonth(): Observable<any> {
    return this.http.get<any>(this.urlGetFeedMonth);

  }

  addFeed(feed: FormData): Observable<any> {
    return this.http.post<any>(this.urlAddFeed, feed);
  }

  deleteEvent(id): Observable<any> {
    const url = `${this.urlAddFeed}/${id}`;
    return this.http.delete(url);
  }

// Get Feed By Id
  getFeedById(id): Observable<any> {
    const url = this.urlGetFeedById + id;
    console.log(url);
    return this.http.get<any>(url);
  }

// Update Feed
  updateFeed(id, data): Observable<any> {
    const url = this.urlUpdateFeedById + id;
    return this.http.post<any>(url, data);
  }


  // Add Feed Ìnormation
  addFeedInformation(data): Observable<any> {
    return this.http.post<any>(this.urlFeedInfor, data);
  }

  // get Feed Information
  getAllFeedInformation(): Observable<any> {
    return this.http.get<any>(this.urlGetFeedInfor);

  }

  deleteFeedInfo(id): Observable<any> {
    const url = this.urlDelFeedInfo + id;
    return this.http.delete(url);
  }

  // Get Feed Infor By Id
  getFeedInfoById(id): Observable<any> {
    const url = this.urlGetFeedInfoById + id;
    console.log(url);
    return this.http.get<any>(url);
  }

  updateFeedInfoById(id, data): Observable<any> {
    const url = this.urlUpdateFeedInfo + id;
    return this.http.post<any>(url, data);
  }

  getRuleFeedInfo(id): Observable<any> {
    const url = this.urlGetRulesFeedInfoById + id;
    return this.http.get<any>(url);
  }

  getDetailRule(id): Observable<any> {
    const url = this.urlGetDetailRule + id;
    return this.http.get<any>(url);
  }

  addRuleFeedInformation(data): Observable<any> {
    return this.http.post(this.urlAddNewRuleFeed, data);
  }

  deleteFeedRule(id): Observable<any> {
    const url = this.urlDeleteFeedRule + id;
    return this.http.delete(url);
  }

  // Update Rule Feed
  updateRuleFeed(id, data): Observable<any> {
    const url = this.urlUpdateRuleFeed + id;
    return this.http.post<any>(url, data);
  }
  // Socket
  eventSocket(data) {
    const socket = io.connect(this.urlSocket);
    console.log(this.urlSocket);
    console.log(data);
    socket.emit('EventTrigger', data);
  }
}
